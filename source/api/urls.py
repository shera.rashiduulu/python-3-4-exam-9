from django.urls import path, include
from rest_framework import routers
from .views import CommentViewSet, LikeView, DislikeView

router = routers.DefaultRouter()
router.register(r'comments', CommentViewSet)

app_name = 'api'

urlpatterns = [
    path('', include(router.urls)),
    path('like/<int:pk>/', LikeView.as_view(), name='like_photo'),
    path('dislike/<int:pk>/', DislikeView.as_view(), name='dislike_photo')
]
