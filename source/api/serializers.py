from rest_framework import serializers
from webapp.models import Comment, Like


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'author', 'photo', 'text', 'created_at')
