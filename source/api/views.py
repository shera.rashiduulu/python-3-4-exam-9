from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import BasePermission
from .serializers import CommentSerializer
from webapp.models import Comment, Photo, Like


class IsAuthorPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in ('DELETE', 'PUT', 'PATCH'):
            return obj.author == request.user or request.user.has_perm('webapp.delete_comment')
        return True


class CommentViewSet(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def get_permissions(self):
        if self.request.method == 'DELETE':
            return [IsAuthorPermission(),]
        else:
            return super().get_permissions()


class LikeView(APIView):
    def post(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        try:
            photo = Photo.objects.get(pk=pk)
        except Photo.DoesNotExist:
            return Response({'error': 'Photo not found!'}, status=404)
        try:
            Like.objects.get(photo=photo, author=request.user)
            return Response({'error': 'You have already liked this photo!'}, status=400)
        except Like.DoesNotExist:
            Like.objects.create(photo=photo, author=request.user)
            photo.likes_count += 1
            photo.save()
            return Response({'id': photo.pk, 'likes_count': photo.likes_count})


class DislikeView(APIView):
    def post(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        try:
            photo = Photo.objects.get(pk=pk)
        except Photo.DoesNotExist:
            return Response({'error': 'Photo not found!'}, status=404)
        try:
            Like.objects.get(photo=photo, author=request.user).delete()
            photo.likes_count -= 1
            photo.save()
            return Response({'id': photo.pk, 'likes_count': photo.likes_count})
        except Like.DoesNotExist:
            return Response({'error': 'You have not liked this photo yet!'}, status=400)
