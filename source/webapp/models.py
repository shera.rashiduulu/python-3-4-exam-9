from django.db import models


class Photo(models.Model):
    photo = models.ImageField(upload_to='images')
    label = models.CharField(max_length=20)
    author = models.ForeignKey('auth.User', on_delete=models.PROTECT, related_name='photos')
    likes_count = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.label}'


class Comment(models.Model):
    text = models.TextField()
    photo = models.ForeignKey('Photo', on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='comments')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.text[:20]}'


class Like(models.Model):
    photo = models.ForeignKey('Photo', on_delete=models.CASCADE, related_name='likes')
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='likes')

    def __str__(self):
        return f'{self.author.username} | {self.photo.label}'

    class Meta:
        unique_together = ('photo', 'author')
