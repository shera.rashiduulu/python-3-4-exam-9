function addComment() {
    let data = {
        text: $('#text').val(),
        author: $('#author').val(),
        photo: $('#photo').val()
    };
    let request = makeRequest('comments', 'post', true,data);
    request.done(function(data, status, response) {
        $('#comment-list').append($(`<div class="card my-3" id="comment-${data.id}">
            <div class="card-body">
                <button type="button" class="close" aria-label="Close" id="comment-delete-${data.id}"
                        data-id="${data.id}">&times;</button>
                <p class="card-text">${data.text}</p>
                <p class="card-subtitle text-muted">${userName} | ${data.created_at}</p>
            </div>
        </div>`));
        $('#comment-delete-' + data.id).click(function(event) {
            event.preventDefault();
            let id = $(event.target).data('id');
            deleteComment(id);
        });
        $('#text').val('');
        $('#comment-save').removeAttr('disabled');
        console.log(data);
    }).fail(function(response, status, message) {
        $('#comment-save').removeAttr('disabled');
        console.log(status);
        console.log(response);
    });
}

function deleteComment(id) {
    let request = makeRequest('comments/' + id, 'delete');
    request.done(function(data, status, response) {
        $('#comment-' + id).remove();
        console.log(data);
    }).fail(function(response, status, message) {
        console.log(status);
        console.log(response);
    });
}

$(document).ready(function() {
    $('#comment-form').submit(function(event) {
        event.preventDefault();
        $('#comment-save').attr('disabled', true);
        addComment();
    });
    $('.close').click(function(event) {
        event.preventDefault();
        let id = $(event.target).data('id');
        deleteComment(id);
    });
});
