from django.contrib import admin
from .models import Photo, Comment, Like


class PhotoAdmin(admin.ModelAdmin):
    fields = ('photo', 'label', 'author', 'likes_count', 'created_at')
    readonly_fields = ('created_at',)
    list_display = ('pk', 'label', 'author', 'created_at', 'likes_count')
    list_display_links = ('pk', 'label')
    list_filter = ('author',)
    ordering = ('-created_at',)


class CommentAdmin(admin.ModelAdmin):
    fields = ('photo', 'author', 'text', 'created_at')
    readonly_fields = ('created_at',)
    list_display = ('pk', '__str__', 'author', 'photo', 'created_at')
    list_display_links = ('pk', '__str__')
    list_filter = ('author', 'photo')
    ordering = ('-created_at',)


class LikeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'author', 'photo')


admin.site.register(Photo, PhotoAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Like, LikeAdmin)
