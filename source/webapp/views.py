from django.views.generic import ListView, DetailView
from .models import Photo, Like
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator


@method_decorator(ensure_csrf_cookie, name='dispatch')
class PhotoListView(ListView):
    model = Photo
    template_name = 'photo_list.html'
    context_object_name = 'photos'


@method_decorator(ensure_csrf_cookie, name='dispatch')
class PhotoDetailView(DetailView):
    model = Photo
    template_name = 'photo_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            try:
                Like.objects.get(author=self.request.user, photo=self.object)
                context['liked'] = True
            except Like.DoesNotExist:
                context['liked'] = False
        return context
